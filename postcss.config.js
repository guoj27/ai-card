module.exports = {
    plugins: [
        require('autoprefixer'),
        require('precss'),
        require('postcss-preset-env'),
        require('postcss-pxtorem')({
            rootValue: 14,
            minPixelValue: 2,
            selectorBlackList: [], //过滤
            propList: ['*'],
        })
    ]
}
