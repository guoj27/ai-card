/**
 * 环境变量
 */
import UAParse from 'ua-parser-js';

/* eslint-disable no-undef */
export const publicPath =  PUBLIC_PATH;
export const deployEnv = DEPLOY_ENV;

/**
 * 获取客户端信息
 */
const ua_parse = new UAParse();
const uaRes = ua_parse.getResult();
export const isAndroid = uaRes.os.name === 'Android';
export const isIOS = uaRes.os.name === 'iOS';
export const isChrome = uaRes.browser.name.includes('Chrome');
export const isSafari = uaRes.browser.name.includes('Safari');
export const isWeChat = uaRes.browser.name === 'WeChat';

/**
 * 获取 AID
 */
export const getAID = () => {
    const AID = 'AIInsuranceTraining001';
    const uatAID = 'AIInsuranceTraining001';
    const aidConfig = {
        dev: uatAID,
        sit: AID,
        uat: uatAID,
        wx: uatAID,
        prod: AID
    };
    return aidConfig[deployEnv];
};

/**
 * 获取 webhook url 前缀
 */
export const getBaseUrl = () => {
    const sitBaseUrl = 'https://dh.spdb-xlab.cn:4431';
    const uatBaseUrl = 'https://dh.spdb-xlab.cn:4430';
    // const uatBaseUrl = 'http://10.190.0.101';
    const baseUrl = {
        dev: uatBaseUrl,
        sit: uatBaseUrl,
        uat: uatBaseUrl,
        wx: uatBaseUrl,
        prod: ''
    };
    return baseUrl[deployEnv];
};

/**
 * 获取 platform websocket url
 */
export const getWebsocketUrl = () => {
    const sitUrl = 'wss://dh.spdb-xlab.cn:4431/digital-human-platform-web-socket';
    const uatUrl = 'wss://x.spdb.com.cn/digital-human-platform-mkt-web-socket';
    const prodUrl = 'wss://x.spdb.com.cn/digital-human-platform-mkt-web-socket';

    const websocketUrl = {
        dev: uatUrl,
        sit: sitUrl,
        uat: uatUrl,
        wx: uatUrl,
        prod: prodUrl
    };
    return websocketUrl[deployEnv];
};

/**
 * 刷新
 */
export const reload = () => {
    window.location.href = window.location.origin + publicPath;
};
