import React from 'react';
import { CSSFade } from 'Components/ui';
import './index.scss';

const loadingPoints = [0, 1, 2];

export default props => {
    return (
        <CSSFade visible={props.visible}>
            <div className="action-loading-container">
                <ul className="point-list">
                    {loadingPoints.map(item => (
                        <li key={item} className={`loading-point-${item}`}></li>
                    ))}
                </ul>
            </div>
        </CSSFade>
    );
};
