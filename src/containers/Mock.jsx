import React, { useCallback, useEffect, useState } from 'react';
import _ from 'lodash';

import { widgetType } from 'Utils/dict';
// import useModal from 'Hooks/useModal';
import Widget from '../widgets';
import widgetQue from '../widgets/_mockData';


export default () => {
    const [widget, setWidget] = useState(widgetQue[0]);
    const [widgetQueIndex, setWidgetQueIndex] = useState(0);

    const sendData = useCallback(() => {
        setWidgetQueIndex(prev => {
            setWidget(widgetQue[prev + 1]);
            return prev + 1;
        });
    }, []);

    useEffect(() => {
        window.sendData = sendData;
        window.sendPrimaryData = sendData;
    }, [sendData]);

    const backPage = useCallback(() => {
        setWidgetQueIndex(prev => {
            setWidget(widgetQue[prev - 1]);
            return prev - 1;
        });
    }, []);

    useEffect(() => {
        window.backPage = backPage;
        window.sendPrimaryData = backPage;
    }, [backPage]);

    useEffect(() => {
        const timeout = parseInt(_.get(widget, 'data.timeout'), 10);

    }, [widget]);

    return (
        <div className="da-entry">
            <div className="da-entry-logo">
                {/* <img src={logoImg} alt="" /> */}
            </div>
            <Widget
                widget={widget}
                subtitle={'测试测试'}
                clearSubtitle={() => {}}
            />
        </div>
    );
};
