/*
* 链接后加上时间戳参数，解决微信的缓存问题
*/

import { publicPath } from 'EnvConfig';

export default () => {
    const urlParam = window.location.href.split('?')[1];
    const redirectUrl = window.location.origin
        + publicPath
        + '#card?v='
        + new Date().getTime()
        + (urlParam ? `&${urlParam}` : '');
    window.location.href = redirectUrl;
    return null;
};
