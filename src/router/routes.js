import React from 'react';
import loadable from '@loadable/component';
import { PageLoading } from 'Components/ui';

const loadFallback = { fallback: <PageLoading /> };
const containers = {
    Redirect: loadable(() => import(/* webpackChunkName: 'Redirect' */'../containers/Redirect'), loadFallback),
    Mock: loadable(() => import(/* webpackChunkName: 'Mock' */'../containers/Mock'), loadFallback),
    Test: loadable(() => import(/* webpackChunkName: 'Topic' */'../widgets/Introducation'), loadFallback),
    NotFound: loadable(() => import(/* webpackChunkName: 'NotFound' */'../containers/NotFound'), loadFallback)
};

export default containers;
