import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

import containers from './routes';


const AppRouter = () => (
    <HashRouter>
        <Switch>
            <Route path={'/'} exact component={containers.Redirect} />
            <Route path={'/card'} component={containers.Mock} />
            <Route path={'/test'} component={containers.Test} />
        </Switch>
    </HashRouter>
);

export default AppRouter;
