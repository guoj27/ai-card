import React, {useRef, useState, useCallback, useEffect} from 'react';
import { Button} from 'antd-mobile';
import api from '../axios/api';
import { isWeChat, publicPath } from 'EnvConfig';

import './chatindex.scss';


export default props => {
  const { data }  = props.widget;

  const chatContent = useRef([]);
  const audioRef = useRef(null);

  const [chatBubble, setChatBubble] = useState([]);
  const [isPlayVoice, setPlayVoice] = useState(false);
  const [isAutoPlay, setIsAutoPlay] = useState(false);
  const [source, setSource] = useState();
  const [curAudio, setCuraudio] = useState();

  useEffect(()=>{
    console.log(window.index,data.pro[window.index]);
  },[window.index])

  const send = (i,item) => {
    chatContent.current = [...chatContent.current, {'right':item},{'left':data.pro[window.index].detailContent[i]}];
    setChatBubble([...chatBubble, {'right':item},{'left':data.pro[window.index].detailContent[i]}]);
    console.log(i,item,chatContent.current,chatContent.current.length)
  }

  const backPage = () => {
    window.backTag = true;
    window.backPage({text:'返回'})
  }

  useEffect(()=>{
    let scrollTarget = document.getElementsByClassName("chat-index")[0];
    scrollTarget.scrollTop = scrollTarget.scrollHeight;
  },[chatContent.current.length])

  const playVoice = (i) => {
    const audio = document.getElementById("audio");
    audio.currentTime = 0;
    setCuraudio(i)
    play();
    setPlayVoice(true);
    setIsAutoPlay(true);
    setSource(data.pro[window.index].recommendWav)
        
  }

  const onAudioPlay = () => {
    
  }

  const onAudioEnded = () => {
    setPlayVoice(false);
  }

  const play = () => {
    audioRef.current && audioRef.current.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause = () => {
    audioRef.current && audioRef.current.pause();
    onAudioEnded();
  };

  useEffect(() => {
    document.addEventListener('visibilitychange', () => {
        const active = document.visibilityState === 'visible';
        console.log('应用正处于前台: ', active ? '是' : '否', document.visibilityState);
        if (active) {
          play();
        } else {
          pause();
        }
    });
  /* eslint-disable */
  }, []);

  return(
    <div className="chat-index">
      <div className="fund-detail">
        <div className="detail-banner">
          <span className="new-tag">新发</span>
          {data.pro[window.index].title}
        </div>
        <div className="fund-item">
          <div>
            基金摘要: <div>{data.pro[window.index].abstract}</div>
          </div>
          <div>
            基金公司: <div>{data.pro[window.index].company}</div>
          </div>
          <div className="bottom-content">
            <div className="summary">
              <div>产品要点:</div>
              {
                data.pro[window.index].summary.map((item,i)=>(
                  <span className="summary-tag" key={i}>{item}</span>
                ))
              }
              <span>近一年收益率</span>
              <div className="rate">{data.pro[window.index].rate}%</div>
            </div>
            <div className="tag">
              {
                data.pro[window.index].tag.map((item,i)=> (
                  <span key={i}>{item}</span>
                ))
              }
            </div>
          </div>
        </div>
      </div>
      
      <div className="chat-bubble">
        <audio
          id="audio"
          autoPlay={isAutoPlay}
          ref={audioRef}
          preload="auto"
          src={source ? `https://spdb.cdn.bcebos.com/AIInsuranceTraining001/main/${source}` : ""}
          onPlay={onAudioPlay}
          onEnded={onAudioEnded}
        />
        { chatContent.current.map((content, i) => (
          <div className="chat-bubble-line" key={i}>
            { Object.keys(content) =='left' && Object.values(content) &&
               <div className="assets-interprete">
                <div className="assets-banner">
                  {
                    content.left.tab === "推荐理由" ?
                    <div className="asset-audio" onClick={() => playVoice(i)}>
                      {/* <img src={require("Imgs/audio.png")} alt=""/>
                      <span className="audio-duration">{content.left.duration}</span> */}
                      <div className={`wifi-symbol ${isPlayVoice ? 'active-audio' : ''}`} >
                          <div className="wifi-circle first"></div>
                          <div className="wifi-circle second" ></div>
                          <div className="wifi-circle third" ></div>
                      </div>
                      <span className="audio-duration">{content.left.duration}</span>
                    </div>
                    : null
                  }
                  
                  <div className="banner-text">{content.left.tab}</div>
                  <img className="banner-icon" src={require("Imgs/asset-log.png")}></img>
                </div>
                <div className="assets-box">
                  {
                    content.left.tab === "推荐理由" ?
                    <div className="reason" dangerouslySetInnerHTML={{__html:content.left.content}}></div>
                    :  content.left.tab === "产品档案" ?
                        <div className="archives">
                          {
                            content.left.list.map((item,i ) => (
                              <div className="archives-item" key={i}>
                                <div className="divwordcolor">{Object.keys(item)}</div>
                                <span className="spanwordcolor">{Object.values(item)}</span>
                              </div>
                            ))
                          }
                        </div>
                          : <div className="managerdata">
                              <div className="divwordcolor">{"基金经理"}</div>
                              <div className="managerdataname">{content.left.proname}</div>
                              <div className="workingyear">{"任职年限：" +content.left.workingyear}</div>
                              <div className="managerdatadetail">
                                {
                                  content.left.list.map((item,i ) => (
                                    <div className="managerdatadetail-item" key={i}>
                                      <div className="divwordcolor">{Object.keys(item)}</div>
                                      <span className="spanwordcolor">{Object.values(item)}</span>
                                    </div>
                                  ))
                                }
                              </div>
                            </div>
                  }
                </div>
              </div>
            }
            { Object.keys(content) =='right' && Object.values(content) &&
              <div className="chat-customer">
                <div className="bubble-content">
                    {Object.values(content)}
                </div>
              </div>}
          </div>
        ))}
      </div>

      <div className="tab-list">
        <span className="tab-item" onClick={backPage}>
          返回
        </span>
        {
          data.pro[window.index].tabList.map((item,i) => (
            <span className="tab-item" key={i} onClick={() => send(i,item)}>{item}</span>
          ))
        }
        <span>
          <a href="tel:13556891235" className="call-tel" >
            呼叫我
          </a>
        </span>
      </div>
    </div>
  )
}