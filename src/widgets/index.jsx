import React, { useState, useEffect, useMemo } from 'react';
import { Icon } from 'antd-mobile';
import _ from 'lodash';

import { widgetType } from 'Utils/dict';
import { ActionLoading } from 'Components/ui';
import Introducation from './Introducation';
import ChatIndex from './Chatindex'

import './index.scss';

// let Radar = null;

export default props => {
    const [curProps, setCurProps] = useState({});
    const [enter, setEnter] = useState(false);

    const curWidgetType = _.get(curProps, 'widget.type', '');

    const componetMap = {
        [widgetType.introducation]: Introducation,
        [widgetType.chatIndex]: ChatIndex
    };

    useEffect(() => {
        // 涉及到其他插件的页面，代码量较大，需要动态加载
        // import(/* webpackChunkName: 'Radar' */'./Radar/List').then(module => {
        //     Radar = module.default;
        // });
    }, []);

    useEffect(() => {
        
        setCurProps(prev => {
            if (_.get(prev, 'widget.type') !== _.get(props, 'widget.type')) {
                setEnter(false);
                setTimeout(() => {
                    setEnter(true);
                    setCurProps(props);
                }, 300);
                return prev;
            }
            else {
                return props;
            }
        });
    }, [props]);

    const wrapperCss = useMemo(() => {
        const fadeCss = enter ? 'fade-in' : 'fade-out';
        return `widget-wrap ${fadeCss}`;
    }, [enter]);

    const InnerWidget = componetMap[curWidgetType];
    if (!InnerWidget) {
        return null;
    }

    return (
        <>
            {curWidgetType && curWidgetType !== widgetType.blank && (
                <div className={wrapperCss}>
                    <InnerWidget {...curProps} />
                    <ActionLoading visible={curProps.actionLoading} />
                </div>
            )}

            {/* for mock */}
            {/* {window.location.href.includes('mock') && (
                <Icon
                    className="wiget-mock-icon"
                    type="right"
                    onClick={window.sendData}
                />
            )} */}
        </>
    );
};
