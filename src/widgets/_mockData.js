/*
 * 保险培训师 Widget 定义
 */

// 音频定义
const audio = {
    audio: {
        customer: {
            audioSource: '音频url',
            audioText: '音频文字'
        },
        dh: {
            audioSource: '音频url',
            audioText: '音频文字',
            displayPosition: 'top',   // top | bottom, 音频播报框是显示在页面顶部还是底部, 不传默认值为 bottom，
            isHelp: true,             // true | false, fase时不显示灯泡以及剩余求助次数, 不传默认为false
            helpTotalCount: 6,
            helpRefAnswer: '参考话术'
        }
    }
};

const introducation = {
    data: {
        empName: '续兴立',
        empAddr: '浦发银行 / 南京分行 私人银行部',
        empTel: '1110',
        assetList: [
            {title: "南京楼市新政全面解读",keyWord:["人才买房新政","人才买房新政","人才买房新政",], audio:"AIInsuranceTraining001dh34_v1.wav",duration:"1'23'' "},
            {title: "南京楼市新政全面解读",keyWord:["人才买房新政","人才买房新政","人才买房新政",], audio:"AIInsuranceTraining001dh32_v1.wav",duration:"0'32''"},
            {title: "南京楼市新政全面解读",keyWord:["人才买房新政","人才买房新政","人才买房新政",], audio:"AIInsuranceTraining001dh31_v1.wav",duration:"32''"},
            {title: "南京楼市新政全面解读",keyWord:["人才买房新政","人才买房新政","人才买房新政",], audio:"AIInsuranceTraining001dh30_v1.wav",duration:"32''"}
        ],
        recommendList: [
            {fundName: '兴全汇吉一年持有期', rate: '40%', tag: ["中风险", "混合基金"],  purchase: "50000"},
            {fundName: '中欧精选灵活配置定期开放', rate: '40%', tag: ["中风险", "混合基金"],  purchase: "50000"},
        ],
        btnShowList: [
            {
                title: '基金投资策略',
                content: '坚持资产配置穿越周期，大类资产配置可选择股债均衡配置的基金；权益类基金建议选择行业均衡配置的基金'
            },
            {
                title: '市场综合观点',
                content: '坚持资产配置穿越周期，大类资产配置可选择股债均衡配置的基金；权益类基金建议选择行业均衡配置的基金'
            },
            
        ]
    },
    type: 'introducation'
};

const chatIndex = {
    data:{
        pro: [
                {
                    title: '兴全汇吉一年持有期',
                    abstract: 'XXXXXXXXXXXXXXXXXXXXXXXXX', //基金摘要
                    company: 'XXX',
                    summary: ['发行12.30-2.5','限额80亿'], //产品要点
                    tag:['人才买房新政','人才买房新政','人才买房新政','人才买房新政', '人才买房新政','人才买房新政','人才买房新政','人才买房新政'],
                    rate: 44,
                    tabList: ['推荐理由','产品档案','基金经理资料'],
                    detailContent: [
                        {
                            tab: '推荐理由',
                            content:"1.陈红女士，拟任基金经理。1981年生，硕士学历。14年证券从业经验，投资风格整体稳健，拥有丰富的大类资产配置经验，专注于“固收+”投资十年以上。擅长通过宏观判断把握大类资产的趋势性机会，自上而下进行大类资产选择；在债券细分领域上，善于把握利率债的波段机会，以及上市公司为主体的信用利差投资机会；</br>2.自资管新规后，市场上银行理财市场产品供给不足，广大理财客户需要风险收益特征比较稳健的替代型产品···",
                            audio:"AIInsuranceTraining001dh34_v1.wav",
                            duration:"1'23"
                        },
                        {
                            tab:'产品档案',
                            list: [{"额度": "100亿"},{"认购时间": '2.2-2.6'},{"产品大类":"定制"},{"产品定位":"定制股债均衡"},{"产品类型":"偏股混合型"}],
                        },
                        {
                            tab:"基金经理资料",
                            proname:'王茜',
                            workingyear:'14',
                            list: [{"目前管理基金数量": "12"},{"基金经理业绩(亿)": '248.67'},{"基金经理业绩":"8.58%"},{"同期大盘年化回报":"4.01%"},{"规模排名":"28/621"}],
                        },
                    ],
                    recommendWav:'AIInsuranceTraining001dh32_v1.wav'
                },
                {
                    title: '中欧精选灵活配置定期开放',
                    abstract: 'XXXXXXXXXXXXXXXXXXXXXXXXX', //基金摘要
                    company: 'XXX',
                    summary: ['发行12.30-2.5','限额80亿'], //产品要点
                    tag:['人才买房新政','人才买房新政','人才买房新政','人才买房新政', '人才买房新政','人才买房新政','人才买房新政','人才买房新政'],
                    rate: 44,
                    tabList: ['推荐理由','产品档案','基金经理资料'],
                    detailContent: [
                        {
                            tab: '推荐理由',
                            content:"1.陈红女士，拟任基金经理。1981年生，硕士学历。14年证券从业经验，投资风格整体稳健，拥有丰富的大类资产配置经验，专注于“固收+”投资十年以上。擅长通过宏观判断把握大类资产的趋势性机会，自上而下进行大类资产选择；在债券细分领域上，善于把握利率债的波段机会，以及上市公司为主体的信用利差投资机会；</br>2.自资管新规后，市场上银行理财市场产品供给不足，广大理财客户需要风险收益特征比较稳健的替代型产品···",
                            audio:"AIInsuranceTraining001dh34_v1.wav",
                            duration:"1'23"
                        },
                        {
                            tab:'产品档案',
                            list: [{"额度": "100亿"},{"认购时间": '2.2-2.6'},{"产品大类":"定制"},{"产品定位":"定制股债均衡"},{"产品类型":"偏股混合型"}],
                        },
                        {
                            tab:"基金经理资料",
                            proname:'王茜',
                            workingyear:'14',
                            list: [{"目前管理基金数量": "12"},{"基金经理业绩(亿)": '248.67'},{"基金经理业绩":"8.58%"},{"同期大盘年化回报":"4.01%"},{"规模排名":"28/621"}],
                        }
                    ],
                    recommendWav:'AIInsuranceTraining001dh34_v1.wav'
                },
        ]
        
    },
    type: 'chatIndex'
}

export default [
    introducation,
    chatIndex,
];


