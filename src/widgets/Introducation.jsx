import React, {useRef, useState, useCallback, useEffect} from 'react';
import { Button} from 'antd-mobile';
import api from '../axios/api';
import { isWeChat, publicPath } from 'EnvConfig';
import {  getUrlParam } from 'Utils';
import videoPoster from 'Imgs/video-poster.png';
import aroundPng from 'Imgs/around.png'

import './index.scss';

let getVideoTime = null;

export default props => {

  const { data }  = props.widget;
  const[btnSrc, setBtnSrc] = useState("time-out");
  const [video, setVideo] = useState();
  const [audio, setAudio] = useState();
  // const [videoTime, setVideoTime] = useState(32);
  const videoTime = useRef(32);
  const [isbtnShow, setIsbtnShow] = useState("none");
  const [showBtnKey,setShowBtnKey] = useState(0);
  const [isPlayVoice, setPlayVoice] = useState(false);
  const [source, setSource] = useState('');
  const [isAutoPlay, setIsAutoPlay] = useState(false);
  const [thisSource, setThisSource] = useState('');
  const [curCAudio, setCurAudio] = useState();
  let [countDown,setCountDown] = useState(0);
  
  const audioRef = useRef(null);
  const videoRef = useRef(null);

  // 注入微信权限验证配置
  const configWX = useCallback(async onError => {
    if(getUrlParam().oldId){
      sessionStorage.setItem('refId', getUrlParam().oldId);
    }else{
      sessionStorage.setItem('refId', "");
    }

    const fetchParams = await api.getPufaWXConfig()       // getPufaWXConfig | getAnluojieWXConfig
      .then(data => data)
      .catch(err => {
          onError(err || '获取微信签名失败');
      });

    if (!fetchParams) {
        return;
    }

    //获取code
    if(fetchParams.appId){
      getcode(fetchParams.appId);
    }

    window.configSuccess = true;

    const configParams = {
        appId: fetchParams.appId,
        jsApiList: ['updateAppMessageShareData'],
        nonceStr: fetchParams.nonceStr,
        signature: fetchParams.signature,
        timestamp: fetchParams.timestamp,
        debug: false
    };

    window.wx.config(configParams);

    window.wx.error(res => {
        console.log('微信配置信息验证失败：', res);
        configSuccess = false;
        setIsWXReady(false);
        // 生产上打开
        onError('微信配置信息验证失败, 请使用浏览器打开网页');
    });
  }, []);

  useEffect(() => {
    if(isWeChat && !window.backTag){
      configWX();
    }
  },[])

  const getcode = (appid) => {
    const url = window.location.href.split('#')[0]+ '?oldId='+ getUrlParam().oldId;
    const code = getUrlParam().code;
    const requestUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' + appid + '&redirect_uri=' + encodeURIComponent(url) +  '&response_type=code&scope=snsapi_base&state=123#wechat_redirect';
    if(!code){
      window.location.href = requestUrl;
    }else if (code) {
      sessionStorage.setItem('code', code);
      getopenId(code)
    }

    return () => sessionStorage.removeItem('code');

    // const fetchCode = await api.getWxCode(fetchParams.appId)
    //     .then(data => data)
    //     .catch(err => {
    //         onError(err || '获取微信code失败');
    //     });
    //     console.log(data)

    // if (!fetchCode) {
    //     return;
    // }
  }

  const getopenId = (code) => {
    const params = {
      refId: sessionStorage.getItem("refId"),
      code: code
    };
    api.getOpenId(params)
      .then(res => {
        if(res.code === 200 ){
          window.openId = res.openId;
          sessionStorage.removeItem('code')
          window.wx.ready(() => {

            if (window.configSuccess) {
              console.log('wx config ready, configSuccess: ', configSuccess, res.openId);
    
                const openId = window.openId;
                let linkUrl = window.origin + publicPath + '?oldId=' + res.openId;
    
                // 需在用户可能点击分享按钮前就先调用
                window.wx.updateAppMessageShareData({
                    title: 'AI名片',
                    desc: '私行专属-周报',
                    link: linkUrl,
                    imgUrl: window.location.origin + publicPath + 'public/lib/redirect-img.png',
                    success: function () {
                        console.log('微信分享设置成功');
                    }
                });
            }
          }); 
        }
      })
      .catch(err => {
          setError(`code获取失败：${err.message}`);
          sessionStorage.removeItem('code')
      })
  }

  useEffect(() => {
    videoRef.current && setVideo(videoRef.current) ;
  },[videoRef])

  useEffect(() => {
    audioRef.current && setAudio(audioRef.current) ;
  },[audioRef])

  const onPlayBtnClick = () => {
    onAudioEnded();
    const videoDom = document.getElementById("intro-video");
    let timer = parseInt(videoDom.duration) - parseInt(videoDom.currentTime);
    setCountDown(!!timer ? timer: parseInt(videoDom.duration));
  
    setTimeout(() => {
      document.getElementsByClassName("video-bg")[0].style.backgroundImage = `url(${aroundPng})`;
    }, 3000);

    if(videoDom.paused) {
      getVideoTime = setInterval(function() {
        setCountDown( timer ? -- timer : 0);
      }, 1000);
      setBtnSrc("play");
      video && video.play();
    }else {
      setBtnSrc("time-out");
      video && video.pause();
      clearInterval(getVideoTime)
    }
  };

  const onVideoPlay = () => {
    setBtnSrc("play");
    video && video.play();
  }

  const onVideoEnded = () => {
    clearInterval(getVideoTime);
    setBtnSrc("time-out");
    video && video.pause();
    document.getElementsByClassName("video-bg")[0].style.backgroundImage = 'url("none")';
  }

  const onVideoPlayError = () => {
    console.error('video play error');
    onVideoEnded();
  }

  const showBtn = key => {
    setIsbtnShow("block");
    setShowBtnKey(key);
  }
  
  const goChat = i => {
    window.sendData({ text: i });
    window.index = i;
  }

  const onPlayAudioClick = (i) => {
    setSource(data.assetList[i].audio);
    const audio = document.getElementById("audio");
    console.log(audio,audio.duration)
    onVideoEnded();
    if(i === curCAudio){
      if(audio.paused){
        setPlayVoice(true);
        setIsAutoPlay(true);
        setCurAudio(i);
        setThisSource(data.assetList[i].audio);
        play();
        
      }else{
        pause();
        setPlayVoice(false);
        setIsAutoPlay(false);
        setThisSource();
        setSource();
      }
    }else{
      setPlayVoice(true);
      setIsAutoPlay(true);
      setCurAudio(i);
      setThisSource(data.assetList[i].audio);
      setSource(data.assetList[i].audio);
      play();
    }
    
  }

  const onAudioPlay = () => {
    audio && audio.play()                                                                                                                                     
  }

  const onAudioEnded = () => {
    setThisSource('');
    setPlayVoice(false);
  }

  const play = () => {
    setPlayVoice(true);
    console.log(audio)
    audio && audio.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause = () => {
    audio && audio.pause();
    setPlayVoice(false);
    // setThisSource('');
    // onAudioEnded();
  };

  useEffect(() => {
    document.addEventListener('visibilitychange', () => {
        const active = document.visibilityState === 'visible';
        console.log('应用正处于前台: ', active ? '是' : '否', document.visibilityState);
        if (active ) {
          play();
        } else {
          pause();
        }
    });
  /* eslint-disable */
  }, []);

  return (
    <div className="ai-card">
      <audio
        id="audio"
        autoPlay={isAutoPlay}
        ref={audioRef}
        preload="auto"
        src={thisSource ? `https://spdb.cdn.bcebos.com/AIInsuranceTraining001/main/${source}` : ""}
        onPlay={onAudioPlay}
        onEnded={onAudioEnded}
      />
      <div className="intro-card">
        <div className="video">
          <div className="video-bg">
            <video
              className="intro-video"
              id="intro-video"
              ref={videoRef}
              src={require('Imgs/intro.mp4')}
              onPlay={onVideoPlay}
              onEnded={onVideoEnded}
              preload="auto"
              onError={onVideoPlayError}
              playsInline
              webkit-playsinline="true"
              x5-video-player-fullscreen="false"
              x5-video-player-type="h5"
              poster={videoPoster}
            />
          </div>
          
          <div className="play-btn" onClick={onPlayBtnClick}>
            <img src={require(`Imgs/${btnSrc}.png`)}/>
            <div className="play-text">{btnSrc === "play" ? "00 : "+ (""+countDown).padStart(2,"0") : "点击播放"}</div>
          </div>
        </div>
        <div className="intro-text">
          <div className="fina-name">{data.empName}</div>
          <div className="fina-addr">{data.empAddr}</div>
          <a className="fina-tel" href="tel:13556891235">
            <img className="tel-img" src={require('Imgs/call.png')}></img>
          </a>
        </div>
      </div>
      <div className="assets-interprete">
          <div className="assets-banner">
            <div className="banner-text"> 私行专属 · 资产配置解读</div>
            <img className="banner-icon" src={require("Imgs/asset-log.png")}></img>
          </div>
          <div className="assets-box">
            <div className="btn-show recommend-item" style={{display: isbtnShow}}>
              <div className="show-title">
                <img src={require(`Imgs/btn${showBtnKey + 1}.png`)} alt=""/>
                {data.btnShowList[showBtnKey].title}
              </div>
              <div className="show-content">
                {
                  showBtnKey === 1 ?
                  <img src={require('Imgs/view-map.png')} alt=""/>
                  :  data.btnShowList[showBtnKey].content
                }
              </div>
            </div>
            
            <ul className="assets-text">
              {
                data.assetList.map((item,i) => (
                  <li className="assets-item" key={i}>
                    <div className="left-asset">
                      <div className="asset-title">{item.title}</div>
                      {
                        item.keyWord.map((item,i) => (
                          <span className="asset-keyword" key={i}>{item}</span>
                        ))
                      }
                    </div>
                    {/* <div className="asset-audio" onClick={() => onPlayAudioClick(i)}>
                      <img src={require("Imgs/audio.png")} alt=""/>
                      <span className={"audio-duration"+i}>{item.duration}</span>
                    </div> */}
                    <div className="asset-audio" onClick={() => onPlayAudioClick(i)}>
                      <div className={`wifi-symbol ${isPlayVoice && curCAudio ===i && thisSource? 'active-audio' : ''}`} >
                          <div className="wifi-circle first"></div>
                          <div className="wifi-circle second" ></div>
                          <div className="wifi-circle third" ></div>
                      </div>
                      <span className={"audio-duration"+i}>{item.duration}</span>
                    </div>
                  </li>
                ))
              }

              <div className="action-btn">
                <div className='btn-cs' onClick={() => showBtn(0)}>
                  <img src={require('Imgs/btn1.png')} alt=""/>
                  基金投资策略
                </div>
                <div className='btn-cs' onClick={() => showBtn(1)}>
                  <img src={require('Imgs/btn2.png')} alt=""/>
                  市场综合观点
                </div>
              </div>
            </ul>
          </div>

          <div className="recommend">
            <img className="recommend-icon" src={require('Imgs/intro-icon.png')} alt=""/>
            <ul className="recommend-content">
              {
                data.recommendList.map((item,i) => (
                  <li className="recommend-item" key={i}>
                    <div className="top-content">
                      <span className="new-tag">新发</span>
                      <span className="fund-name">{item.fundName}</span>
                    </div>
                    <div className="bottom-cotent">
                      <div className="rate-box">
                        <div>{item.rate}</div>
                        <div>近一年收益率</div>
                      </div>
                      <div className="fund-tag">
                        {
                          item.tag.map((item,i) => (
                            <span className="border-box" key={i}>{item}</span>
                          ))
                        }
                        <div className="border-box purchase">最低申购: ${item.purchase}</div>
                      </div>
                      <div className="arrow" onClick={() => goChat(i)}>
                        <img src={require('Imgs/arrow.png')} alt=""/>
                      </div>
                    </div>
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
    </div>
  );
};
