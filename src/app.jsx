import React from 'react';
import ReactDOM from 'react-dom';
import initReactFastclick from 'react-fastclick';

import AppRouter from './router';
import { getAID } from 'EnvConfig';

import './styles/app.scss';

const App = () => <AppRouter />;

ReactDOM.render(<App />, document.getElementById('app'));
initReactFastclick();

const appInitial = () => {
    window.AID = getAID();
    window.dhen = {
        recordRequestIds: [],   // 用于分段录音
        isAppVisible: true
    };
};

appInitial();
