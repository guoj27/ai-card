import React from 'react';

import { Button } from 'Components/ui';

export default props => {
    const { body, confirmText, onConfirm } = props.data;
    return (
        <div className="da-modal">
            <div className="content">{body}</div>
            {confirmText && (
                <div className="err-footer">
                    <Button onClick={onConfirm}>
                        {confirmText}
                    </Button>
                </div>
            )}
        </div>
    );
};
