import React, { useCallback, useEffect } from 'react';
import { Modal } from 'antd-mobile';

import ErrorModal from './ErrorModal';
import ExitModal from './ExitModal';
import Popup from './Popup';
import { reload } from 'EnvConfig';

import './index.scss';

let erModal = null;
let exModal = null;
let poModal = null;

export default () => {
    const onErrorClose = useCallback(() => {
        if (erModal) {
            erModal.close();
            erModal = null;
        }
    }, []);

    const onExitClose = useCallback(() => {
        if (exModal) {
            exModal.close();
            exModal = null;
        }
    }, []);

    const onPopupClose = useCallback(() => {
        if (poModal) {
            poModal.close();
            poModal = null;
        }
    }, []);

    useEffect(() => {
        return () => {
            onErrorClose();
            onExitClose();
            onPopupClose();
        };
    }, [onErrorClose, onExitClose, onPopupClose]);

    const onError = useCallback((msg, isClose = true) => {
        if (erModal) {
            return;
        }
        const confirmText =  '确定';
        const onConfirm = () => {
            if (isClose) {
                reload();
            } else {
                onErrorClose();
            }
        };
        const errorData = {
            body: msg,
            confirmText,
            onConfirm
        };
        const msgBody = <ErrorModal data={errorData} />;
        erModal = Modal.alert(undefined, msgBody, []);
    }, [onErrorClose]);

    const onExitWarning = useCallback(() => {
        if (exModal) {
            return;
        }
        const msgBody = <ExitModal onClose={onExitClose} />;
        exModal = Modal.alert(undefined, msgBody, []);
    }, [onExitClose]);

    const onPopup = useCallback(popupData => {
        if (poModal) {
            return;
        }
        const msgBody = <Popup data={popupData} onClose={onPopupClose} />;
        poModal = Modal.alert(undefined, msgBody, []);
    }, [onPopupClose]);

    return {
        onError,
        onExitWarning,
        onPopup
    };
};

// 调用方式
// const { onError, onExitWarning, onPopup } = useModal();
// onExitWarning();
// onError('小浦发现您上次意外退出， 您可以选择继续，或重新开始', 1008);
// onPopup({
//     title: '',
//     body: '小浦发现您上次意外退出， 您可以选择继续，或重新开始',
//     confirmText: '继 续',
//     cancelText: '重新开始'
// });
