import useARecorder from './useARecorder';
import useCRecorder from './useCRecorder';
import useWXRecorder from './useWXRecorder';

export default isWXReady => {
    if (isWXReady) {
        return useWXRecorder;
    }
    return useARecorder;
};
