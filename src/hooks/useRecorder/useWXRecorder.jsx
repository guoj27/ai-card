/*
* 微信录音
*/
import { useCallback, useEffect } from 'react';
import { Toast } from 'antd-mobile';

import { getUUID } from 'Utils';

let speakStartTime = 0;
let recordRequestId = null;

export default handleRecordAutoEnd => {
    const uploadAudio = useCallback(localId => {
        window.wx.uploadVoice({
            localId,
            isShowProgressTips: 0,            // 默认为1，显示进度提示
            success(res) {
                const serverId = res.serverId;    // 返回音频的服务器端ID
                console.log('微信录音音频上传成功, serverId: ', serverId);
                window.sendPrimaryData({
                    format: 'amr',
                    type: 'wx_audio',
                    mediaId: serverId,
                    wxId: 'pufa01',      // pufa01 | anluojie
                    requestId: recordRequestId
                }, false);
            }
        });
    }, []);

    useEffect(() => {
        // 录音时间超过一分钟没有停止的时候会执行 complete 回调
        window.wx.onVoiceRecordEnd({
            complete(res) {
                console.log('onRecordEnded res: ', res);
                const localId = res.localId;
                uploadAudio(localId);
                handleRecordAutoEnd();
            }
        });
    }, [uploadAudio, handleRecordAutoEnd]);

    const startRecord = useCallback(e => {
        recordRequestId = getUUID();
        window.dhen.recordRequestIds.push(recordRequestId);
        window.wx.startRecord();
        speakStartTime = new Date().getTime();
    }, []);

    const stopRecord = useCallback(() => {
        const speakTime = new Date().getTime() - speakStartTime;
        if (speakTime < 1000) {
            Toast.fail('录音时间太短了');
            window.wx.stopRecord();
            return;
        }

        window.wx.stopRecord({
            success(res) {
                console.log('stop success: ', res);
                const localId = res.localId;
                uploadAudio(localId);
            },
            fail(res) {
                Toast.fail('录音失败');
                console.error(res);
            }
        });
    }, [uploadAudio]);

    return {
        startRecord,
        stopRecord
    };
};
