import CryptoJS from 'crypto-js/crypto-js';

/**
 * 容错性JSON转换
 */
export const CustomJSON = {
    parse: str => {
        let obj;
        try {
            obj = JSON.parse(str);
        }
        catch (e) {
            console.error('JSON.parse Error:', e);
        }
        return obj || undefined;
    },
    stringify: obj => {
        let str;
        try {
            str = JSON.stringify(obj);
        }
        catch (e) {
            console.error('JSON.stringify error:', e);
        }
        return str || '';
    }
};

/**
 * 休眠
 */
export const sleep = time => {
    return new Promise(resolve => setTimeout(resolve, time));
};

/**
 * 获取地址链接中的参数
 */
export const getUrlParam = () => {
    const paramStr = window.location.href.split('?')[1];
    if (!paramStr) {
        return {};
    }
    const paramMaps = paramStr.split('&');
    const param = {};
    paramMaps.forEach(item => {
        const arr = item.split('=');
        param[arr[0]] = arr[1];
    });
    return param;
};

/**
 * 字符混淆
 */
export const encrypt = word => {
    const srcs = CryptoJS.enc.Utf8.parse(word);
    const key = CryptoJS.enc.Utf8.parse('1234567890123456');
    const encrypted = CryptoJS.AES.encrypt(srcs, key, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
};

/**
 * 解决js浮点数乘法运算不精确的问题
 */
export const accMul = (arg1, arg2) => {
    let m = 0;
    const s1 = arg1.toString();
    const s2 = arg2.toString();
    try {
        m += s1.split('.')[1].length;
    }
    catch (e) {}

    try {
        m += s2.split('.')[1].length;
    }
    catch (e) {}

    return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m);
};

/**
 * 计算时间段
 */
export const transTime = dateTimeStamp => {
    const minute = 60 * 1000;
    const hour = minute * 60;
    const day = hour * 24;
    // const halfamonth = day * 15;
    const month = day * 30;
    const now = new Date().getTime();
    const diffValue = now - dateTimeStamp;
    if (diffValue < 0) {
        return;
    }
    const monthC = diffValue / month;
    const weekC = diffValue / (7 * day);
    const dayC = diffValue / day;
    const hourC = diffValue / hour;
    const minC = diffValue / minute;

    let result = null;

    if (monthC >= 1) {
        // eslint-disable-next-line radix
        result = '' + parseInt(monthC) + '月前';
    } else if (weekC >= 1) {
        // eslint-disable-next-line radix
        result = '' + parseInt(weekC) + '周前';
    } else if (dayC >= 1) {
        // eslint-disable-next-line radix
        result = '' + parseInt(dayC) + '天前';
    } else if (hourC >= 1) {
        // eslint-disable-next-line radix
        result = '' + parseInt(hourC) + '小时前';
    } else if (minC >= 1) {
        // eslint-disable-next-line radix
        result = '' + parseInt(minC) + '分钟前';
    } else {
        result = '刚刚';
    }
    // console.log(result)
    return result;
};

/**
 * 获取唯一字符串
 */
export const getUUID = () => {
    const s4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    return (s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4());
};
