import jsonp from 'jsonp';
import { Toast } from 'antd-mobile';
import axios from './index';

/**
 * 获取累计使用人数
 */
const getUserCount = () => {
    return axios.get(`/api/digitalhuman/aitrainer/count/${window.AID}`)
        .then(res => res)
        .catch(err => Toast.fail(err.message || '人数统计失败'));
};

/**
 * 获取图形验证码
 */
const getValidatecode = () => {
    return axios.get('/api/digitalhuman/aitrainer/getImageCode').then(res => res);
};

/**
 * 本地开发登录, 不含验证码
 */
const devLogin = param => {
    return axios.post('/api/digitalhuman/aitrainer/login', param).then(res => res);
};

/**
 * 测试及生产环境登录, 含验证码
 */
const login = param => {
    return axios.post('/api/digitalhuman/aitrainer/loginWithCode', param).then(res => res);
};

/**
 * 获取浦发银行公众号微信权限配置参数
 */
const getPufaWXConfig = () => {
    const url = window.location.href.split('#')[0];
    const requestUrl = `/api/digitalhuman/aitrainer/wx/getSignature?url=${encodeURIComponent(url)}`;
    return axios.get(requestUrl)
        .then(res => {
            if (res) {
                return res;
            }
            throw Error();
        });
};
/**
 * 获取安络杰公众号微信权限配置参数
 */
const getAnluojieWXConfig = () => {
    const url = window.location.href.split('#')[0];

    let requestUrl = 'https://validation.energytrust.com.cn/v1/weixin/jssdk';
    requestUrl += `?url=${encodeURIComponent(url)}`;
    requestUrl += '&api=startRecord,stopRecord,onVoiceRecordEnd,uploadVoice,updateAppMessageShareData,';
    requestUrl += '&debug=true';

    return new Promise((resolve, reject) => {
        jsonp(
            requestUrl,
            { timeout: 30 * 1000 },
            (err, data) => {
                console.log('jsonp callback: ', data);
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            }
        );
    });
};

const getWxCode = (appid) => {
    let requestUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    let url = window.location.href;
    requestUrl += `?appid=${appid}`;
    requestUrl += `&redirect_uri=${encodeURIComponent(url)}`;
    requestUrl += '&response_type=code&scope=snsapi_base&state=123#wechat_redirect';

    console.log("requestUrl",requestUrl)

    return new Promise((resolve, reject) => {
        jsonp(
            requestUrl,
            { timeout: 30 * 1000 },
            (err, data) => {
                console.log('jsonp callback: ', data);
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            }
        );
    });
}

const getOpenId = param => {
    return axios.get('/api/digitalhuman/aitrainer/wx/getOpenId', param).then(res => res);
};

export default {
    getUserCount,
    getValidatecode,
    login,
    devLogin,
    getPufaWXConfig,
    getAnluojieWXConfig,
    getWxCode,
    getOpenId
};
